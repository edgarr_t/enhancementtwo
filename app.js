const express = require('express');
const bodyParser = require('body-parser');
const stock = require('./routes/stocks.route'); // Imports routes for the products
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://127.0.0.1:27017/market';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/stocks', stock);

global.__root   = __dirname + '/'; 

var UserController = require(__root + 'controllers/users.controller');
app.use('/stocks/api/users', UserController);

var AuthController = require(__root + 'controllers/auth.controller');
app.use('/stocks/api/auth', AuthController);


let port = 1234;
app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});
