import json
from json import dumps
from bson import json_util
from pymongo import MongoClient
import sys
import bottle
from bottle import route, run, request, abort, post, get, put, delete ,response

connection = MongoClient('localhost', 27017)
db = connection['market']
collection = db['stocks']

# Beginnning of Support functions
def getMovingAverageCount(min, max):
  result = collection.find({ "50-Day Simple Moving Average":
    {
      '$gte': min ,
      '$lte': max
    }
  }).count()
  return result

def industryMatch(string):
  result = collection.find(
  {
    "Industry" : string
  }  ).limit(5)
  return result 

def getSector(string):
  pipline = [
    { '$match': { "Sector": string}},
    { '$group': { "_id" : "$Industry" , "Shares Outstanding" :  {"$sum": "$Shares Outstanding"}} }
  ]
  result = collection.aggregate(pipline)
  return result 
  
def delete_document(key, value):
  result = collection.delete_one({key:value})
  if not result:
    abort(404, 'No document with %s : %s' % key,value) 
  return result

def update_document(key, value,document):
  result = collection.update({key:value},{ '$set': document }, upsert=False,multi=False) 
  if not result:
    abort(404, 'No document with %s : %s' % key,value)
  return json.loads(json.dumps(result, indent=4, default=json_util.default))

def get_document(key, value):
  document = collection.find_one({key:value})
  if not document:
    abort(404, 'No document with %s : %s' % key,value)
  return document

def insert_document(document):
  try:
    result = collection.save(document)
  except (ValidationError) as ve:
    abort(400, str(ve))
#  return result
# End of support functions

# Begin of API def


@put('/stocks/api/v1.0/updateStock/<sym>')
def update_stock(sym):
  try:
    result = request.json
    update_document("Ticker", sym, result)
  except error:
    abort(404,'Error')   
  print "Complete"

@post('/stocks/api/v1.0/createStock/<sym>')
def post_stock(sym):
    try:
      result = request.json
      data = insert_document(result)
      print(data)
    except error:
      abort(404,'Error')
#    return result

@post('/stocks/api/v1.0/stockReport')
def post_stockReport():
    list = []
    try:
      result = request.body
      for i in result:
        if len(i) > 0:
          string = i
      print(string)
      string = string.replace('[',"")
      string = string.replace(']',"")
      symbols = string.split(',')
      for i in symbols:
        print(i)
        list.append(get_document("Ticker",i))
        
    except error:
      abort(404,'Error')
    return str(list)

@delete('/stocks/api/v1.0/deleteStock/<sym>')
def delete_stock(sym):
  try:  
    result = delete_document("Ticker", sym)
  except NameError:
    abort(404, 'No parameter')
  return bottle.HTTPResponse(status=200)


@route('/stocks/api/v1.0/getStock/<sym>')
def get_stock(sym):  
    try:
      string = get_document("Ticker", sym)
    except NameError:
        abort(404, 'No parameter')
    return json.loads(json.dumps(string, indent=4, default=json_util.default))



@route('/stocks/api/v1.0/portfolio/<sym>')
def get_portfolio(sym):  
    list = []
    try:
      result = collection.find( {'$text': {'$search':sym }})
      for i in result:
        list.append(i)
    except NameError:
        abort(404, 'No parameter')
    return str(list)#json.loads(json.dumps(string, indent=4, default=json_util.default))
  
@route('/stocks/api/v1.0/industryReport/<sym>')
def get_industryReport(sym):  
    list = []
    try:
      result = industryMatch(sym)
      for i in result:
        list.append(i)
    except NameError:
        abort(404, 'No parameter')
    return str(list)#json.loads(json.dumps(string, indent=4, default=json_util.default))
    
  
if __name__ == '__main__': #declare instance of request
    #app.run(debug=True)
    run(host='localhost', port=8080)


         